// import Vue from 'vue'
// import Vuetify from 'vuetify'
// import colors from 'vuetify/es5/util/colors'

// Vue.use(Vuetify, {
//   theme: {
//     primary: '#B0BEC5',
//     accent: '#607D8B',
//     secondary: '#75816B',
//     info: '#1A237E',
//     warning: '#FDD835',
//     error: '#DD2C00',
//     success: '#75816B'
//   }
// })

import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify)
